import random
import time

import numpy as np
from sklearn.metrics import accuracy_score
from sklearn.neural_network import MLPClassifier
import threading

MAX_VALUES = 500

models = {}
scores = {}


def print_data(tab):
    file = open('result.sav', "w")

    for tmp in tab:
        file.write(str(tmp) + " / ")

    file.write("\n")


class MonThread(threading.Thread):
    def __init__(self, model, x_train, x_test, y_train, y_test, pos, modele):
        threading.Thread.__init__(self)
        self.model = model
        self.x_train = x_train
        self.x_test = x_test
        self.y_train = y_train
        self.y_test = y_test
        self.pos = pos
        self.etat = False
        self.modele = modele

    def run(self):
        self.etat = True
        # print("Début thread n°" + str(self.pos))
        self.model.fit(self.x_train, self.y_train)

        y_pred = self.model.predict(self.x_test)
        tmp = accuracy_score(y_true=self.y_test, y_pred=y_pred)

        models[self.pos] = self.modele
        scores[self.pos] = tmp
        # print("Accuracy " + str(self.pos) + " :  " + str(tmp))
        print("Fin thread n°" + str(self.pos))
        self.etat = False


class Model:

    def __init__(self):
        self.alpha = random.uniform(.2, .0001)
        self.batch_size = random.randint(50, 500)
        self.epsilon = random.uniform(1e-09, 1e-07)
        self.hidden_layer_sizes = random.randint(50, 600)
        self.max_iter = random.randint(100, 5000)

    def to_string(self):
        return str(self.alpha) + " " + str(self.batch_size) + " " + str(self.epsilon) + " " + str(
            self.hidden_layer_sizes) \
            + " " + str(self.max_iter)

    def mutation(self):
        val = self.alpha * (10 / 100)
        self.alpha = random.uniform(self.alpha - val, self.alpha + val)

        val = self.batch_size * (10 / 100)
        self.batch_size = random.randint(int(self.batch_size - val), int(self.batch_size + val))

        val = self.epsilon * (10 / 100)
        self.epsilon = random.uniform(self.epsilon - val, self.epsilon + val)

        val = self.hidden_layer_sizes * (10 / 100)
        self.hidden_layer_sizes = random.randint(int(self.hidden_layer_sizes - val), int(self.hidden_layer_sizes + val))

        val = self.max_iter * (10 / 100)
        self.max_iter = random.randint(int(self.max_iter - val), int(self.max_iter + val))


        return self


def opti(x_train, x_test, y_train, y_test):
    file = open('result.sav', 'w')

    models_tmp = []
    # Première génération
    for z in range(MAX_VALUES):
        a = Model()
        models_tmp.append(a)
        file.write(a.to_string() + " / ")

    file.write("\n")

    models_tmp = np.array(models_tmp)

    best = generation(models_tmp, x_train, x_test, y_train, y_test)

    for z in range(50):
        models_tmp = create_next_generation(best)

        file.write(str(z) + " = ")

        for a in models_tmp:
            file.write(a.to_string() + " / ")

        best = generation(models_tmp, x_train, x_test, y_train, y_test)


def generation(models_tmp, x_train, x_test, y_train, y_test):
    for r in range(0, len(models_tmp), 10):

        list_threads = []
        for n in range(10):
            model = MLPClassifier(
                alpha=models_tmp[r + n].alpha,
                batch_size=models_tmp[r + n].batch_size,
                epsilon=models_tmp[r + n].epsilon,
                hidden_layer_sizes=(models_tmp[r + n].hidden_layer_sizes,),
                learning_rate='adaptive',
                max_iter=models_tmp[r + n].max_iter
            )

            tmp = MonThread(model, x_train, x_test, y_train, y_test, r + n, models_tmp[r + n])
            tmp.start()
            list_threads.append(tmp)

        is_all_end = False

        while not is_all_end:
            time.sleep(1)
            for tmp in list_threads:
                if tmp.etat:
                    is_all_end = False
                    break
                else:
                    is_all_end = True

    scores_sorted = sorted(scores)

    # file = open('result.sav', "w")

    az = list(map(models.get, scores_sorted[:int(MAX_VALUES / 4)]))
    # az2 = list(map(models_tmp.get, scores_sorted[:int(MAX_VALUES / 4)]))

    # for r in az:
    #     file.write(r.to_string() + " / ")

    return az


def create_next_generation(past_generation):
    scores = {}
    models = {}

    current_generation = []

    for model in past_generation:

        current_generation.append(model)

        # On creer une mutation
        for i in range(3):
            current_generation.append(model.mutation())

    return current_generation


if __name__ == '__main__':

    file = open('result.sav')

    models_tmp = []
    # Première génération
    for z in range(50):
        models_tmp.append(Model())

    for i in range(50):
        for a in range(3):
            models_tmp.append(models_tmp.__getitem__(i).mutation())

    models_tmp = np.array(models_tmp)
