import matplotlib.pyplot as plt
import numpy as np

plt.style.use('_mpl-gallery')

# make data
x = np.linspace(0, 100, 25)
y = np.array([0, 1, 0, 0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1])

print(x)

plt.scatter(x, y)

plt.show()

#
# # plot
# fig, ax = plt.subplots()
#
# ax.plot(y, x, linewidth=2.0)
# #
# # ax.set(xlim=(0, 8),
# #        ylim=(0, 8))
#
# plt.show()
