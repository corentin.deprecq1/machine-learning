import os

import joblib
import numpy as np
import sys

from src.Emotion_Project.sound import extract_data


def analyse():
    filename = 'model.sav'
    exist = os.path.getsize(filename) != 0

    if exist and len(sys.argv) > 1:
        feature = []
        model = joblib.load(open(filename, 'rb'))

        tmp = extract_data(sys.argv[1])

        feature.append(tmp)

        prediction = model.predict(np.array(feature))

        return prediction[0]

    return "ERROR"


if __name__ == '__main__':
    print(analyse())
