import glob
import os

import joblib
import librosa
import numpy as np
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPClassifier
from tqdm import tqdm

import Optimisation

emotions = {
    '01': 'NEUTRAL',
    '02': 'CALM',
    '03': 'HAPPY',
    '04': 'SAD',
    '05': 'ANGRY',
    '06': 'FEARFUL',
    '07': 'DISGUST',
    '08': 'SURPRISED'
}


def read_data(test_size=0.2):
    x, y = [], []
    for file in tqdm(glob.glob("./../../dataset/*/Actor_*/*.wav")):
        file_name = os.path.basename(file)
        emotion = emotions[file_name.split("-")[2]]

        feature = extract_data(file)

        x.append(feature)

        y.append(emotion)

    # return np.array(x), y
    return train_test_split(np.array(x), y, test_size=test_size, random_state=5)


def extract_data(file):
    y, sr = librosa.load(file, duration=3, offset=.5)

    stft = np.abs(librosa.stft(y))

    result = np.array([])

    mfccs = np.mean(librosa.feature.mfcc(y=y, sr=sr, n_mfcc=40).T, axis=0)
    result = np.hstack((result, mfccs))

    chroma = np.mean(librosa.feature.chroma_stft(S=stft, sr=sr).T, axis=0)
    result = np.hstack((result, chroma))

    mel = np.mean(librosa.feature.melspectrogram(y=y, sr=sr).T, axis=0)
    result = np.hstack((result, mel))

    return result


if __name__ == '__main__':
    test_size = 0.2

    filename = 'model.sav'
    isNotSaved = os.path.getsize(filename) == 0

    # On lit les données
    # x_train, y_train = read_data(test_size=test_size)

    if isNotSaved:
        x_train, x_test, y_train, y_test = read_data(test_size=test_size)

        print("Nombre de données d'entrainement : " + str(x_train.shape[0]))

        # Optimisation.opti(x_train, x_test, y_train, y_test)

        # On cree le modele d'apprentissage
        model = MLPClassifier(
            alpha=0.01,
            batch_size=256,
            epsilon=1e-08,
            hidden_layer_sizes=(300,),
            learning_rate='adaptive',
            max_iter=1000
        )

        # On entraine le modele avec les données d'entrainement
        model.fit(x_train, y_train)

        joblib.dump(model, filename)
    else:
        model = joblib.load(open(filename, 'rb'))

    x, y = [], []

    for file in glob.glob("./../../dataset/pres/*wav"):
        feature = []
        file_name = os.path.basename(file)

        tmp = extract_data(file)

        feature.append(tmp)

        emotion = emotions[file_name.split("-")[2]]

        prediction = model.predict(np.array(feature))

        x.append(tmp)
        y.append(emotion)
        print("Fichier                        : " + file)
        print("Emotion trouvée par la machine : " + str(prediction[0]))
        print("Emotion réelle                 : " + emotion + "\n")

    if isNotSaved:
        y_pred = model.predict(x_test)
        accuracy = accuracy_score(y_true=y_test, y_pred=y_pred)
    else:
        y_pred = model.predict(np.array(x))
        accuracy = accuracy_score(y_true=np.array(y), y_pred=y_pred)

    print("Accuracy: {:.2f}%".format(accuracy * 100))
