import time
import pandas as pd
import seaborn as sns
import numpy as np
from sklearn.neighbors import KNeighborsClassifier


def test_custom_value(m):
    pclass = input("Saisir la classe de votre personne : [0, ..., n] ")
    sex = input("Saisir le sex de votre personne : [0 (homme), 1 (femme)] ")
    age = input("Saisir l'age de votre personne : [0, ..., n] ")

    x = np.array([int(pclass), int(sex), int(age)]).reshape(1, 3)

    score = m.predict(x)
    affiche_predict(pclass, sex, age, score)


def affiche_predict(pclass, sex, age, score):
    print("Etant en classe " + str(pclass) + ", " + ("Madame" if sex == 0 else "Monsieur") + " est " + (
        'décédé(e)' if score[0] == 0 else " toujours là ") + " à l'age de " + str(age) + " ans.")


def test_value(model):
    file = pd.read_excel("../dataset/test.xlsx")

    for i, line in file.iterrows():
        x = np.array([line["pclass"], line["sex"], line["age"]]).reshape(1, 3)
        score = model.predict(x)
        affiche_predict(line["pclass"], line["sex"], line["age"], score)


if __name__ == '__main__':

    titanic = sns.load_dataset('titanic')

    titanic = titanic[['survived', 'pclass', 'sex', 'age']]

    titanic.dropna(axis=0, inplace=True)
    titanic['sex'].replace(['male', 'female'], [0, 1], inplace=True)

    titanic.head()

    model = KNeighborsClassifier()

    y = titanic['survived']
    X = titanic.drop('survived', axis=1)

    model.fit(X.values, y.values)

    while True:
        print()
        print("======= MENU DE SELECTION =======")
        print("1: Voir la prédiction")
        print("2: Faire un test avec un jeu de test")
        print("3: Faire un test avec des valeurs personnalisées")
        print("QUIT: Quitter")
        print()

        val = input("Saisir une valeur : ")

        if val == "1":
            print(model.score(X.values, y.values))
        elif val == "2":
            test_value(model)
        elif val == "3":
            test_custom_value(model)
        elif val == "QUIT":
            exit(0)

        time.sleep(1)
