# Machine-learning - Mémoire

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/corentin.deprecq1/machine-learning.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/corentin.deprecq1/machine-learning/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

## Ce que vous trouverez ici
Dans ce repository vous trouverez les algorithmes d'entrainements et les datasets qui ont pu servir à la réalisation du mémoire de Deprecq Corentin et Chapuis Lucas. 

## Stack technique
Tout est basé sur l'utilisation de la librairie Sci-kit learn et le langage python. Dans 2 autres repository vous trouverez les codes sources de l'API web et de l'IHM réalisés en Flask et React.

## Objectifs
Proposer d'une part un code source clair et lisible qui pourra être repris sur une instance local avec un minimum d'installation et d'autre part montrer comment il est possible d'intégrer ces algorithmes à un projet plus concret et professionnel.

## Installation
Pour utiliser ce projet voici les librairies dont vous aurez besoin : 
- [ ] [Scikit Learn](https://scikit-learn.org/)
- [ ] [Numpy](https://numpy.org/)
- [ ] [Librosa](https://librosa.org/doc/latest/index.html)
- [ ] [Soundfile](https://pypi.org/project/soundfile/)
- [ ] [Pandas](https://pandas.pydata.org/)

Commande d'import de toutes les librairies : 
```py
pip install librosa soundfile numpy sklearn pyaudio pandas seaborn
```

## Usage
Poc_Titanic
Simplement lancer le programme en vous plaçant dans son répertoire et en executant la commande :
```py
py main.py
```
Puis laissez vous guider par la petite IHM en ligne de commande afin d'effectuer vos tests